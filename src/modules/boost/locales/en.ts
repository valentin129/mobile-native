export type TranslationType = typeof en;

const en = {
  dayWithCount: '{{count}} day',
  dayWithCount_plural: '{{count}} days',
  tokenWithCount: '{{count}} token',
  tokenWithCount_plural: '{{count}} tokens',
};

export default en;
