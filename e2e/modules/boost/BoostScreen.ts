import AppScreen from '../common/AppScreen';

class BoostScreen extends AppScreen {
  constructor() {
    super('BoostScreen');
  }
}

export default new BoostScreen();
